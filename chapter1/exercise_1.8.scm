; This exercise demonstrates to us custom cubic root procedure

; good-enough uses difference between 'guess'
; and 'prev-guess' values, it is the same procedure as it was
; in 1.7 exercise
(define (good-enough guess prev-guess)
	(< (abs (- guess prev-guess)) 0.001))

; simple procedure to find average of three values 'x' and 'y' and 'y'
; as you can see there is two 'y' values now, it is a necessity for cubic
; root evaluation
(define (average x y)
	(/ (+ x y y) 3))

; procedure that improves our 'guess' according to the algorithm
; this procedure along with 'average' are what changed from the 1.7 exercise.
; here we use new formula for cubic root.
(define (improve guess x)
	(average (/ x (square guess)) guess))

; recursive procedure that takes initial 'guess', previous guess 'prev-guess' and 'x'. 
; The procedure calculates cubic root of 'x' parameter.
(define (curt-iter guess prev-guess x)
	(if (good-enough guess prev-guess)
		guess
		(curt-iter (improve guess x)
				   guess
				   x)))

; We assume 1.0  and 2.0 values as a initial arguments to 'curt-iter' procedure.
; This way we can just pass one argument 'x' which should be processed.
(define (custom-curt x)
	(curt-iter 1.0 2.0 x))

; Some tests. Cubic root in this case will be found with some error.
; The answer is 3.0000000000000977.
(custom-curt 27)
; the root in this case will be 2.6575822250321525e-3 and this is interesting
; because this value is very close to the true answer. I'm not sure why this happened
; Formula is quite similiar to what was used in square root exercise, but we could not
; calculate square root of a very small number that time. Now we can calculate cubic
; root of a very small number!
(custom-curt 0.00000001)
; the answer in this case will be 464158.88336127787 which is very close to the true
; result. Nothing really interesting here, all action is happening in the previous test.
(custom-curt 100000000000000000)