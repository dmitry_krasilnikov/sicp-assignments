; This procedure sums two parameters, the second parameter is 
; taken by its absolute value.
; as SICP book states evaluation allows for combinations
; whose operators are compound expressions. We can observe
; that depending on 'b' parameter 'a' and 'b' will be summed
; or subtructed in this case. 
(define (a-plus-abs-b a b)
		((if (> b 0) + -) a b))

; some showcases
(a-plus-abs-b 2 2) ; 4
(a-plus-abs-b 2 -2) ; 4