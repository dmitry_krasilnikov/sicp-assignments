; here we define procedure that retuns itself
(define (p) (p))

; then we define test procedure which will behaviour defferently
; depending on evaluation order that is used in a language
(define (test x y)
		(if (= x 0)
		0
		y))

; when we're trying to evaluate test procedure Scheme's interpreter
; hangs because of infinite recursion. This happens because of applicative-order
; of evaluation in Scheme's interpreter. All arguments are evaluated before
; the beggining of the 'test' procedure's evaluation. 'p' procedure returns
; itself that is why recursion happens. If interpreter would have normal-order evaluation
; then 'test' procedure would be evaluated before its arguments and it wouldn't be
; necessary to evaluate (p) combination. Because of special form 'if' and 0 argument of
; 'test' procedure, 'test' would return 0.
(test 0 (p))