; This is not a working program, just an example from the book

(define (new-if predicate then-clause else-clause)
        (cond (predicate then-clause)
              (else else-clause)))


(define (sqrt-iter guess x)
        (new-if (good-enough guess x)
                guess
                (sqrt-iter (improve guess x) x)))

; 'new-if' procedure would not work as expected because Scheme interpreter
; has applicative order of execution and all arguments must be evaluated
; before executing the procedure. In our case this means that in order 
; to execute 'new-if' procedure we must first evaluate 'good-enough', 'guess',
; and 'sqrt-iter'. Although there is nothing wrong with first two parameters,
; 'sqrt-iter' procedure will take us to the infinite recusrion and the whole
; thing wouldn't work.
