; Here is example from the book. How to calculate square root
; using Newton's method of successive approximations

; good-enough procedure later will be used to identify
; whether our 'guess' is good enough and we should stop calculating
(define (good-enough guess x)
	(< (abs (- x (square guess))) 0.001))

; simple procedure to find average of two values 'x' and 'y'
(define (average x y)
	(/ (+ x y) 2))

; procedure that improves our 'guess' according to the algorithm
(define (improve guess x)
	(average (/ x guess) guess))

; recursive procedure that takes initial 'guess' and 'x'. 
; The procedure calculates square root of 'x' parameter.
(define (sqrt-iter guess x)
	(if (good-enough guess x)
		guess
		(sqrt-iter (improve guess x)
			x)))

; We assume 1.0 value as a initial argument to 'sqrt-iter' procedure.
; This way we can just pass one argument 'x' which should be processed.
(define (custom-sqrt x)
	(sqrt-iter 1.0 x))

; Some tests. Square root in this case will be found with some error.
; The answer is 3.00009155413138
(custom-sqrt 9)
; the root in this case will be 0.04124542607499115 which gives us
; value of 0.0017011.. when squared. I think that such answer is not
; close enough so is not acceptable
(custom-sqrt 0.001)
; the root won't be evaluated and interpreter will hang 
; because of a very large numbers that should be exponentiated 
; during good-enough procedure
(custom-sqrt 100000000000000000)
