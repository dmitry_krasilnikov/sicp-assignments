; sum of squares of x and y => x^2 + y^2
; we define this procedure using builtin 'sqaure' procedure
(define (square-sum x y)
		(+ (square x) (square y)))

; procedure takes three parameters and returns sum of squares
; of two largest of them
(define (f a b c) 
		(cond ((and (<= a b) (<= a c)) (square-sum b c)) 
			  ((and (<= b a) (<= b c)) (square-sum a c)) 
			  (else (square-sum a b))))

; some tests
(f 1 2 3) ; 13
(f 3 3 3) ; 18
(f 3 1 2) ; 13
