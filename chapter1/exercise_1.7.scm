; It is a rewritten variant of 1.1.7 example

; in this version good-enough uses difference between 'guess'
; and 'prev-guess' values, we will see how it changes whole picture
; in the tests section
(define (good-enough guess prev-guess)
	(< (abs (- guess prev-guess)) 0.001))

; simple procedure to find average of two values 'x' and 'y'
(define (average x y)
	(/ (+ x y) 2))

; procedure that improves our 'guess' according to the algorithm
(define (improve guess x)
	(average (/ x guess) guess))

; recursive procedure that takes initial 'guess', previous guess 'prev-guess' and 'x'. 
; The procedure calculates square root of 'x' parameter.
(define (sqrt-iter guess prev-guess x)
	(if (good-enough guess prev-guess)
		guess
		(sqrt-iter (improve guess x)
				   guess
				   x)))

; We assume 1.0  and 2.0 values as a initial arguments to 'sqrt-iter' procedure.
; This way we can just pass one argument 'x' which should be processed.
(define (custom-sqrt x)
	(sqrt-iter 1.0 2.0 x))

; Some tests. Square root in this case will be found with some error.
; The answer is 3.00009155413138. Actually the result is the same
; as in the 1.1.7 example
(custom-sqrt 9)
; in this case the root is 0.03162278245070105 which is better than it previously was
; (0.04124542607499115 value from 1.1.7 example), becuase it produces an answer of
; 0.0010000003699244 when squared which is quite acceptable as I see it.
(custom-sqrt 0.001)
; the answer in this case will be 316227766.01683795 which is very close to the true
; result. We can now calculate a root of a very big number, it is much better
; because in the 1.1.7 example 'good-enough' procedure was using 'square' operation
; that led to out of integer type situation and hanging of interpreter.
(custom-sqrt 100000000000000000)
